using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace NoDontDoIt.DataBinding
{
    
    public sealed class DataBinder<T> : IDataBinder<T>
    {
        
        private readonly List<IBinding<T>> _bindings;
        private Func<T> _getter;

        public DataBinder(Func<T> getter = null)
        {
            _bindings = new List<IBinding<T>>();
            _getter = getter;
        }

        public bool CanDispose => (_getter == null) && (_bindings.Count == 0);

        public void AddGetter(Func<T> getter)
        {
            if (getter == null) throw new ArgumentNullException(nameof(getter));
            if (_getter != null) throw new InvalidOperationException("Getter is already assigned.");
            
            _getter = getter;
            
            MarkDirty();
        }

        public void AddBinding(IBinding<T> binding)
        {
            if (binding == null) throw new ArgumentNullException(nameof(binding));
            
            _bindings.Add(binding);
            binding.MarkDirty();
        }

        public void MarkDirty()
        {
            if (_getter == null) return;
            foreach (IBinding<T> binding in _bindings) binding.MarkDirty();
        }

        public void RebindDirty(ILogger logger)
        {
            for (int i = 0; i < _bindings.Count;)
            {
                IBinding<T> binding = _bindings[i];

                if (!binding.IsDirty)
                {
                    ++i;
                    continue;
                }

                if (TryGet(_getter, logger, out T value))
                {
                    TryBind(binding, value, logger);

                    for (++i; i < _bindings.Count; ++i)
                    {
                        binding = _bindings[i];

                        if (binding.IsDirty) TryBind(binding, value, logger);
                    }
                }

                break;
            }
        }
        
        public bool RemoveGetter()
        {
            if (_getter == null) return false;
            
            _getter = null;

            return true;
        }

        public bool RemoveBinding(IBinding<T> binding)
        {
            return _bindings.Remove(binding);
        }
        
        private static bool TryGet(Func<T> getter, ILogger logger, out T value)
        {
            try
            {
                value = getter();
                return true;
            }
            catch (Exception exception)
            {
                logger.LogError(exception, $"Failed to retrieve value using getter.");
                value = default;
                return false;
            }            
        }

        private static void TryBind(IBinding<T> binding, T value, ILogger logger)
        {
            try
            {
                binding.Bind(value, logger);
            }
            catch (Exception exception)
            {
                logger.LogError(exception, "Failed while binding value.");
            }
        }

    }
    
}