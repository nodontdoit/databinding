using NoDontDoIt.DataBinding.Collections;

namespace NoDontDoIt.DataBinding
{
    
    public interface IRootBindingContext : IBindingContext
    {
        
        ICommandCollection Commands { get; }
        void ExecuteCommand(string id, object sender);
        void ExecuteCommand<TValue>(string id, object sender, TValue value);

    }
    
}