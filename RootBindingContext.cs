using System;
using System.Collections.Generic;
using NoDontDoIt.DataBinding.Collections;

namespace NoDontDoIt.DataBinding
{
    
    public class RootBindingContext : BindingContext, IRootBindingContext
    {

        public ICommandCollection Commands { get; }

        private readonly Dictionary<string, Delegate> _commandActions;

        public RootBindingContext(IViewModel viewModel = null)
            : base(viewModel)
        {
            _commandActions = new Dictionary<string, Delegate>();
            
            Commands = new CommandCollection(_commandActions);
        }
        
        public void ExecuteCommand(string id, object sender)
        {
            var command = (Action<object>)_commandActions[id];
            command(sender);
        }

        public void ExecuteCommand<TValue>(string id, object sender, TValue value)
        {
            var command = (Action<object, TValue>)_commandActions[id];
            command(sender, value);
        }
        
    }
    
}