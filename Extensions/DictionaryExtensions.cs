using System.Collections.Generic;

namespace NoDontDoIt.DataBinding.Extensions
{
    
    internal static class DictionaryExtensions
    {

        public static bool TryGetValue<TKey, TValue, TCastedValue>(this IDictionary<TKey, TValue> dictionary, TKey key, out TCastedValue castedValue) where TCastedValue : TValue
        {
            if (dictionary.TryGetValue(key, out TValue value))
            {
                castedValue = (TCastedValue)value;
                return true;
            }

            castedValue = default;
            return false;
        }
        
    }
    
}