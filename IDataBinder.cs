using System;
using Microsoft.Extensions.Logging;

namespace NoDontDoIt.DataBinding
{
    
    public interface IDataBinder
    {

        bool CanDispose { get; }
        void MarkDirty();
        void RebindDirty(ILogger logger);
        bool RemoveGetter();

    }

    public interface IDataBinder<TProperty> : IDataBinder
    {

        void AddGetter(Func<TProperty> getter);
        void AddBinding(IBinding<TProperty> binding);
        bool RemoveBinding(IBinding<TProperty> binding);

    }
    
}