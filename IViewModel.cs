using System.ComponentModel;
using NoDontDoIt.DataBinding.Collections;

namespace NoDontDoIt.DataBinding
{
    
    public interface IViewModel : INotifyPropertyChanged
    {
        
        void Declare(IPropertyCollection propertyCollection);

    }
    
}