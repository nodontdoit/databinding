using Microsoft.Extensions.Logging;
using NoDontDoIt.DataBinding.Collections;

namespace NoDontDoIt.DataBinding
{
    
    public interface IBindingContext
    {
        
        IViewModel ViewModel { get; set; }
        IBindingCollection Bindings { get; }
        void TryMarkDirty(string name);
        void RebindDirty(ILogger logger);

    }
    
}