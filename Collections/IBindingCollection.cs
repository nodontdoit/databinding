namespace NoDontDoIt.DataBinding.Collections
{
    
    public interface IBindingCollection
    {

        void Add<TProperty>(string propertyName, IBinding<TProperty> binding);
        bool Remove<TProperty>(string propertyName, IBinding<TProperty> binding);

    }
    
}