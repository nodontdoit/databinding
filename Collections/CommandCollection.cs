using System;
using System.Collections.Generic;

namespace NoDontDoIt.DataBinding.Collections
{
    
    public sealed class CommandCollection : ICommandCollection
    {

        private readonly Dictionary<string, Delegate> _items;

        public CommandCollection(Dictionary<string, Delegate> items)
        {
            _items = items;
        }

        public void Add(string id, Action<object> handler) => _items.Add(id, handler);

        public void Add<TValue>(string id, Action<object, TValue> handler) => _items.Add(id, handler);

        public bool Remove(string id) => _items.Remove(id);

    }
    
}