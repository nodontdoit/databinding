using System;

namespace NoDontDoIt.DataBinding.Collections
{
    
    public interface ICommandCollection
    {

        void Add(string id, Action<object> handler);
        void Add<TValue>(string id, Action<object, TValue> handler);
        bool Remove(string id);

    }
    
}