using System;
using System.Collections.Generic;
using NoDontDoIt.DataBinding.Extensions;

namespace NoDontDoIt.DataBinding.Collections
{
    
    public sealed class PropertyCollection : IPropertyCollection
    {
        
        private readonly Dictionary<string, IDataBinder> _binders;

        public PropertyCollection(Dictionary<string, IDataBinder> binders)
        {
            _binders = binders;
        }

        public void Add<TProperty>(string name, Func<TProperty> getter)
        {
            if (_binders.TryGetValue(name, out IDataBinder<TProperty> binder))
            {
                binder.AddGetter(getter);
            }
            else
            {
                binder = new DataBinder<TProperty>(getter);
                _binders.Add(name, binder);
            }
        }

        public bool Remove(string name)
        {
            IDataBinder binder = _binders[name];
            bool result = binder.RemoveGetter();
            
            if (binder.CanDispose) _binders.Remove(name);

            return result;
        }

        public void Clear()
        {
            var toRemove = new List<string>(_binders.Count);

            foreach (var kvp in _binders)
            {
                string name = kvp.Key;
                IDataBinder binder = kvp.Value;
                
                binder.RemoveGetter();
                
                if (binder.CanDispose) toRemove.Add(name);
            }

            foreach (string name in toRemove) _binders.Remove(name);
        }
        
    }
    
}