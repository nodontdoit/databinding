using System.Collections.Generic;
using NoDontDoIt.DataBinding.Extensions;

namespace NoDontDoIt.DataBinding.Collections
{
    
    public sealed class BindingCollection : IBindingCollection
    {
        
        private readonly Dictionary<string, IDataBinder> _binders;

        public BindingCollection(Dictionary<string, IDataBinder> binders)
        {
            _binders = binders;
        }
        
        public void Add<TProperty>(string propertyName, IBinding<TProperty> binding)
        {
            if (_binders.TryGetValue(propertyName, out IDataBinder<TProperty> binder))
            {
                binder.AddBinding(binding);
            }
            else
            {
                binder = new DataBinder<TProperty>();
                binder.AddBinding(binding);
                
                _binders.Add(propertyName, binder);
            }
        }

        public bool Remove<TProperty>(string propertyName, IBinding<TProperty> binding)
        {
            var binder = (IDataBinder<TProperty>)_binders[propertyName];
            bool result = binder.RemoveBinding(binding);
            
            if (binder.CanDispose) _binders.Remove(propertyName);

            return result;
        }

    }
    
}