using System;

namespace NoDontDoIt.DataBinding.Collections
{
    
    public interface IPropertyCollection
    {

        void Add<TProperty>(string name, Func<TProperty> getter);
        bool Remove(string name);
        void Clear();

    }
    
}