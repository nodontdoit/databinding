using Microsoft.Extensions.Logging;

namespace NoDontDoIt.DataBinding
{

    public interface IBinding
    {
        
        bool IsDirty { get; }
        void MarkDirty();

    }
    
    public interface IBinding<in TProperty> : IBinding
    {
        
        void Bind(TProperty value, ILogger logger);

    }
    
}