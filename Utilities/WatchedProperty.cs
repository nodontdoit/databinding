using System;

namespace NoDontDoIt.DataBinding.Utilities
{
    
    public sealed class WatchedProperty<T>
    {

        public readonly string Name;
        
        private T _value;
        private readonly Action<string> _valueChanged;

        public WatchedProperty(string name, Action<string> valueChanged, T value = default)
        {
            Name = name;
            
            _valueChanged = valueChanged;
            _value = value;
        }

        public T Value
        {
            get => _value;
            set
            {
                _value = value;
                _valueChanged?.Invoke(Name);
            }
        }

    }
    
}