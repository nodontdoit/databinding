using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Extensions.Logging;
using NoDontDoIt.DataBinding.Collections;

namespace NoDontDoIt.DataBinding
{
    
    public class BindingContext : IBindingContext
    {
        
        public IBindingCollection Bindings { get; }
        
        private readonly Dictionary<string, IDataBinder> _binders;
        private readonly IPropertyCollection _properties;

        private IViewModel _viewModel;

        public BindingContext(IViewModel viewModel = null)
        {
            _binders = new Dictionary<string, IDataBinder>();
            _properties = new PropertyCollection(_binders);
            
            Bindings = new BindingCollection(_binders);
            ViewModel = viewModel;
        }

        public IViewModel ViewModel
        {
            get => _viewModel;
            set
            {
                if (_viewModel == value) return;
                
                if (_viewModel != null)
                {
                    _viewModel.PropertyChanged -= OnPropertyChanged;
                    _properties.Clear();
                }
                
                _viewModel = value;

                if (_viewModel != null)
                {
                    _viewModel.Declare(_properties);
                    _viewModel.PropertyChanged += OnPropertyChanged;
                }
            }
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            TryMarkDirty(args.PropertyName);
        }
        
        public void TryMarkDirty(string name)
        {
            if (!_binders.TryGetValue(name, out IDataBinder binder)) return;
            binder.MarkDirty();
        }
        
        public void RebindDirty(ILogger logger)
        {
            foreach (IDataBinder binder in _binders.Values) binder.RebindDirty(logger);
        }
        
    }
    
}